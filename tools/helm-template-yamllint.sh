#!/usr/bin/env bash

set -e
set -o pipefail

CHART_DIR=${CHART_DIR:-$1}
YAMLLINT_CONFIG="${YAMLLINT_CONFIG:-/tools/common/configuration/yamllint-helm.yaml}"

if [[ -z ${CHART_DIR} ]]; then
  echo "Missing parameter.

  This script expect to find either:

  CHART_DIR environment variable defined with the path of the chart to validate

  helm-template-yamllint.sh \$CHART_DIR"

  exit 1
fi
script_dir=$(dirname "$(realpath "$0")")

if [ ! -f "$YAMLLINT_CONFIG" ]; then
    echo -e "\e[0Ksection_start:`date +%s`: the config file $YAMLLINT_CONFIG not exsiting\r\e[0K---------------  the config file $YAMLLINT_CONFIG  is not existing"
    echo -e "\e[0Ksection_end:`date +%s`:the config file $YAMLLINT_CONFIG not exsiting\r\e[0K"
    exit 1
fi

function helm() { $(which helm) $@ 2> >(grep -v 'found symbolic link' >&2); }

chart_name=$(yq -e .name $CHART_DIR/Chart.yaml )

echo -e "\e[0Ksection_start:`date +%s`:helm_dependency_update[collapsed=true]\r\e[0K--------------- helm dependency update"
helm dependency update $CHART_DIR
echo -e "\e[0Ksection_end:`date +%s`:helm_dependency_update\r\e[0K"

if [[ $chart_name == "sylva-units" ]]; then
  units_default_enabled=$(yq '[.units | ... comments="" | to_entries | .[] | select((.value.enabled != null) and (.value.enabled == true or .value.enabled == "yes"))] | map(.key)' $CHART_DIR/values.yaml)
  if [[ $units_default_enabled != '[]' ]]; then
    echo -e "The following units have .enabled defined to 'true' in 'values.yaml':\n$units_default_enabled\n\n"
    echo "This is not allowed in sylva-units 'values.yaml':"
    echo "  - in 'values.yaml' all units are disabled by default, and we don't set 'units.xxx.enabled: true' there anymore"
    echo "  - when there is some conditionality to decide to enable a unit or not:"
    echo "    * either this is about necessary conditions, in that case units.xxx.enabled_conditions needs to be used"
    echo "    * or this is about wanting to have a unit be enabled _by default_ conditionally in the mgmt cluster:"
    echo "      in this case the condition will be put in 'units.xxx.enabled', but in 'management.values.yaml'"
    echo "  - 'workload-cluster.values.yaml' is the place where to set 'units.xxx.enabled' for units that we want to enable in workload clusters"
    echo
    exit -1
  fi

    helm dependency update $CHART_DIR

    echo -e "\e[0Ksection_end:`date +%s`:helm_dependency_update\r\e[0K"

    echo -e "\e[0Ksection_start:`date +%s`:helm_base_values\r\e[0K--------------- Checking default values with 'helm template' and 'yamllint' (for sylva-units chart all units enabled) ..."

    # This applies only to sylva-units chart where we want to check that templating
    # works fine with all units enabled
    yq eval '{"units": .units | ... comments="" | to_entries | map({"key":.key,"value":{"enabled":true,"enabled_conditions":[]}}) | from_entries}' $CHART_DIR/values.yaml > /tmp/all-units-enabled.yaml


  helm template ${chart_name} $CHART_DIR --values /tmp/all-units-enabled.yaml \
  | yamllint - -c  "${YAMLLINT_CONFIG}"

  echo OK
  echo -e "\e[0Ksection_end:`date +%s`:helm_base_values\r\e[0K"
  if [ -d $CHART_DIR/test-values ]; then
      test_dirs=$(find $CHART_DIR/test-values -mindepth 1 -maxdepth 1 -type d)
      if [ -n "$test_dirs" ] ; then
        for dir in $test_dirs ; do
          # This checks whether `helm template` should be run with .Release.IsUpgrade set instead of .Release.IsInstall
          if [[ -f $dir/test-spec.yaml && $(yq .release-is-upgrade $dir/test-spec.yaml) == "true" ]]; then
              HELM_FLAG=" --is-upgrade"
          else
              HELM_FLAG=""
          fi

          echo -e "\e[0Ksection_start:`date +%s`:helm_more_values\r\e[0K--------------- Checking values from test-values/$(basename $dir) with 'helm template ${HELM_FLAG}' and 'yamllint' ..."

          set +e
          helm template ${chart_name}${HELM_FLAG} $CHART_DIR $(ls $dir/*.y*ml | sort | grep -v test-spec.yaml | sed -e 's/^/--values /') \
            | yamllint - -c  "${YAMLLINT_CONFIG}"
          exit_code=$?
          set -e

          if [[ -f $dir/test-spec.yaml && $(yq .require-failure $dir/test-spec.yaml) == "true" ]]; then
              expected_exit_code=1
              error_message="This testcase is supposed to make 'helm template ..${HELM_FLAG}| yamllint ..' fail, but it actually succeeded."
              success_message="This negative testcase expectedly made 'helm template ..${HELM_FLAG}| yamllint ..' fail."
          else
              expected_exit_code=0
              error_message="Failure when running 'helm template ..${HELM_FLAG}| yamllint ..' on this test case."
          fi

          if [[ $exit_code -ne $expected_exit_code ]]; then
            echo $error_message
            exit 1
          else
            echo $success_message
          fi

          echo OK
          echo -e "\e[0Ksection_end:`date +%s`:$dir\r\e[0K"
        done
      fi
    fi
else
  if [ -d $CHART_DIR/test-values ]; then
    test_dirs=$(find $CHART_DIR/test-values -mindepth 1 -maxdepth 1 -type d)
    if [ -n "$test_dirs" ] ; then
      for dir in $test_dirs ; do
        echo -e "\e[0Ksection_start:`date +%s`:$dir[collapsed=true]\r\e[0K--------------- Checking values from test-values/$(basename $dir) with 'helm template' and 'yamllint' ..."

        set +e
        helm template ${chart_name} $CHART_DIR $(ls $dir/*.y*ml | grep -v test-spec.yaml | sed -e 's/^/--values /') \
          | yamllint - -c  "${YAMLLINT_CONFIG}"
        exit_code=$?
        set -e

        if [[ -f $dir/test-spec.yaml && $(yq .require-failure $dir/test-spec.yaml) == "true" ]]; then
            expected_exit_code=1
            error_message="ERROR - This testcase is supposed to make 'helm template ..| yamllint ..' fail, but it actually succeeded."
            success_message="OK - This negative testcase expectedly made 'helm template ..| yamllint ..' fail."
        else
            expected_exit_code=0
            error_message="ERROR - Failure when running 'helm template ..| yamllint ..' on this test case."
            success_message="OK"
        fi

        if [[ $exit_code -ne $expected_exit_code ]]; then
          echo $error_message
          exit 1
        else
          echo $success_message
        fi
        echo -e "\e[0Ksection_end:`date +%s`:$dir\r\e[0K"
      done
    fi
  fi
fi
if [[ $chart_name == "sylva-units" ]]; then
  for value_file in $CHART_DIR/values.yaml $(ls $CHART_DIR/*.values.yaml); do
    echo -e "\e[0Ksection_start:`date +%s`:additional_check_$value_file[collapsed=true]\r\e[0K--------------- checking patches/components/postRenderers in $value_file ..."
    echo "--- "
    postRenderers=$(yq eval '[.units | to_entries | .[] | select(.value.helmrelease_spec.postRenderers)] | map(.key) | join(", ")' $value_file)
    if [[ -n $postRenderers ]]; then
      echo "Some units defined in $value_file use helmrelease_spec.postRenderers, although they should use helmrelease_spec._postRenderers instead: $postRenderers"
      exit 1
    fi

    components=$(yq eval '[.units | to_entries | .[] | select(.value.kustomization_spec.components)] | map(.key) | join(", ")' $value_file)
    if [[ -n $components ]]; then
      echo "Some units defined in $value_file use kustomization_spec.components, although they should use kustomization_spec._components instead: $components"
      exit 1
    fi

    patches=$(yq eval '[.units | to_entries | .[] | select(.value.kustomization_spec.patches)] | map(.key) | join(", ")' $value_file)
    if [[ -n $patches ]]; then
      echo "Some units defined in $value_file use kustomization_spec.patches, although they should use kustomization_spec._patches instead: $patches"
      exit 1
    fi

    echo -e "\e[0Ksection_end:`date +%s`:additional_check_$value_file\r\e[0K"
  done
fi
