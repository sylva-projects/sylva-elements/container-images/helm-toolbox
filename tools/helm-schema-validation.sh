#!/usr/bin/env bash
set -e

CHART_DIR=${CHART_DIR:-$1}
YAMLLINT_CONFIG="${YAMLLINT_CONFIG:-/tools/common/configuration/yamllint-helm.yaml}"
if [[ -z ${CHART_DIR} ]]; then
  echo "Missing parameter.

  This script expect to find either:

  CHART_DIR environment variable defined with the path name of the chart to validate

  helm-schema-validation.sh \$CHART_DIR
  "
  exit 1
fi

function helm() { $(which helm) $@ 2> >(grep -v 'found symbolic link' >&2); }

script_dir=$(dirname "$(realpath "$0")")
chart_name=$(yq -e .name $CHART_DIR/Chart.yaml )

if [ ! -f "$YAMLLINT_CONFIG" ]; then
    echo -e "\e[0Ksection_start:`date +%s`: the config file $YAMLLINT_CONFIG not exsiting\r\e[0K---------------  the config file $YAMLLINT_CONFIG  is not existing"
    echo -e "\e[0Ksection_end:`date +%s`:the config file $YAMLLINT_CONFIG not exsiting\r\e[0K"
    exit 1
fi

RED='\033[0;31m'
NC='\033[0m'
echo -e "\e[0Ksection_start:`date +%s`:lint_schema\r\e[0K--------------- Lint Schema YAML file"
echo "Lint $CHART_DIR/values.schema.yaml ..."
yamllint --no-warnings -c "${YAMLLINT_CONFIG}" $CHART_DIR/values.schema.yaml
echo "   DONE"
echo -e "\e[0Ksection_end:`date +%s`:lint_schema\r\e[0K"

echo -e "\e[0Ksection_start:$(date +%s):check_generation\r\e[0K--------------- Check that ${CHART_DIR}/values.schema.json was regenerated from values.schema.yaml ..."

if [[ $chart_name == "sylva-units" ]]; then
  if [[ -n "${CI_JOB_NAME:-}" ]]; then
      export PREFIX_PATH="tools/"
    else
      export PREFIX_PATH="/vol/tools"
    fi
    ${PREFIX_PATH}/generate_json_schema.py -o /tmp/values.schema.json
  if ! cmp -s /tmp/values.schema.json $CHART_DIR/values.schema.json ; then
    echo -e "${RED}$CHART_DIR/values.schema.json wasn't generated with $CHART_DIR/values.schema.yaml${NC}"
    exit 1
  fi
else
  $script_dir/yaml2json.py < $CHART_DIR/values.schema.yaml > /tmp/values.schema.json
  if ! cmp -s /tmp/values.schema.json $CHART_DIR/values.schema.json ; then
    echo -e "${RED}$CHART_DIR/values.schema.json wasn't generated with$CHART_DIR/values.schema.yaml${NC}"
    echo -e "${RED}you need to run this: yaml2json.py < values.schema.yaml > values.schema.json${NC}"
    exit 1
  fi
fi

echo "   DONE"
echo -e "\e[0Ksection_end:`date +%s`:check_generation\r\e[0K"

echo -e "\e[0Ksection_start:`date +%s`:validate_schema\r\e[0K--------------- Validate that the chart values schema contains a valid JSON Schema ..."
check-jsonschema --check-metaschema $CHART_DIR/values.schema.json
echo "   DONE"
echo -e "\e[0Ksection_end:`date +%s`:validate_schema\r\e[0K"
