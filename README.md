# helm-toolbox

## Bash Script Summary

### Objective
The objective of these tools is to help validation of Helm charts: validate and lint YAML files, generate JSON schemas, and validate the JSON schemas against the JSON Schema specification.

### Script Overview
- It compares the generated JSON schema with the existing one and displays an error message if they differ.
- It validates the generated JSON schema against the JSON Schema specification using `check-jsonschema`.

#### Usage
you can use the wrapper script `helm-validate.sh` to facilitate helm checking on [`sylva-core`](https://gitlab.com/sylva-projects/sylva-core) and [`sylva-capi-cluster`](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster) project this way
```bash
./helm-validate.sh <chart-directory> schema-validation
./helm-validate.sh <chart-directory> yamllint
```

#### Options

- `CHART_DIR`: Path to be checked (required).
- `YAMLLINT_CONFIG`: Yamllint file configuration (option) and by default `tools/common/configuration/yamllint-helm.yaml`
- `TEST_VALUES`: default values file tested by `helm lint` 
- `SCRIPT_VALIDATION`: optionnal script for creating validation schema
- `ADDITIONNAL_REPO_CHARTS` :  default values is empty otherwise list for example like this "bitnami=https://charts.bitnami.com/bitnami apache-airflow=https://airflow.apache.org"


The scripts above can be easily included in a GitLab pipeline, utilizing a specified Docker image located in [`registry.gitlab.com/sylva-projects/sylva-elements/container-images/helm-toolbox`](https://gitlab.com/sylva-projects/sylva-elements/container-images/helm-toolbox/container_registry/6301606).
