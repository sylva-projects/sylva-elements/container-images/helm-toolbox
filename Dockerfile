FROM alpine:3.21.3

ARG TARGETOS=linux
ARG TARGETARCH=amd64

# renovate: datasource=github-releases depName=helm/helm VersionTemplate=v
ENV HELM_VERSION="3.17.1"
# renovate: datasource=github-releases depName=mikefarah/yq VersionTemplate=v
ENV YQ_VERSION="4.45.1"
# renovate: datasource=github-releases depName=helm-unittest/helm-unittest VersionTemplate=v
ENV HELM_UNITTEST_VERSION="0.8.0"
# renovate: datasource=github-releases depName=chartmuseum/helm-push VersionTemplate=v
ENV HELM_PUSH_VERSION="0.10.4"


ENV PATH="${PATH}:/tools"

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

# hadolint ignore=DL3018, SC1091
RUN apk update \
    && apk add --no-cache build-base python3 py3-pip pipx jq wget curl bash yamllint check-jsonschema py3-yaml git \
    && wget -q --show-progress --progress=bar:force https://get.helm.sh/helm-v${HELM_VERSION}-${TARGETOS}-${TARGETARCH}.tar.gz -O - | tar -xzO ${TARGETOS}-${TARGETARCH}/helm > /usr/local/bin/helm \
    && wget -q --show-progress --progress=bar:force https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_${TARGETOS}_${TARGETARCH} -O /usr/local/bin/yq \
    && chmod +x /usr/local/bin/helm /usr/local/bin/yq \
    && helm plugin install https://github.com/helm-unittest/helm-unittest --version $HELM_UNITTEST_VERSION \
    && helm plugin install https://github.com/chartmuseum/helm-push.git --version $HELM_PUSH_VERSION \
    && mkdir /vol \
    && apk del git

WORKDIR /tools

COPY tools/ .

CMD ["/usr/local/bin/helm version"]
